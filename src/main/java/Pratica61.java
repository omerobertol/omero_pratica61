import java.util.Set;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * @author Omero Francisco Bertol <chicobertol@gmail.com>
 */

public class Pratica61 {

  public static void main(String[] args) {
    Time time1 = new Time();
    Time time2 = new Time();
    
    time1.addJogador("Goleiro", new Jogador(1, "Fulano"));
    time1.addJogador("Lateral", new Jogador(4, "Ciclano"));
    time1.addJogador("Atacante", new Jogador(10, "Beltrano"));

    time2.addJogador("Goleiro", new Jogador(1, "João"));
    time2.addJogador("Lateral", new Jogador(7, "José"));
    time2.addJogador("Atacante", new Jogador(15, "Mário"));
    
    System.out.println("Posição    Time 1        Time 2");
    Set<String> keysTime = time1.getJogadores().keySet();
    keysTime.forEach((key) -> {
        System.out.println(key + " " + time1.getJogadores().get(key).toString() + " " + time2.getJogadores().get(key).toString());
      });
  }
 
}