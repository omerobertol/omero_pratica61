package utfpr.ct.dainf.if62c.pratica;

import java.util.HashMap;

public class Time {
    
  private final HashMap<String, Jogador> jogadores;

  public Time() {
    this.jogadores = new HashMap<>();
  }

  public HashMap<String, Jogador> getJogadores() {
    return jogadores;
  }
  
  public void addJogador(String pos, Jogador jog) {
    this.jogadores.put(pos, jog);
  }
    
}